# syntax = edrevo/dockerfile-plus
INCLUDE+ .Dockerfile.base

# Instalar los programas necesarios
RUN apt-get update && apt-get install -y wget iputils-ping curl jp2a
RUN pip install ultralytics

# Configuracion de la aplicación
ENV TERM=xterm
ENV COLORTERM=24bit
COPY ["src/", "/app/"]
WORKDIR /app
ENTRYPOINT ["/app/main.sh"]
VOLUME /imagenes
