#!/bin/bash

# Este script simplemente debe chequear que haya conexión a internet.
# Asegúrese de retornar un valor de salida acorde a la situación.
# Puede que necesite modificar el archivo Dockerfile.

# -q: deshabilita la salida en pantalla que envìe el script
# --spider: no accede a la página, solo chequea su disponibilidad
# $?: retorna el codigo de estado del ultimo comando
# wget: comando que permite descargar archivos o contenido directamente de internet

wget -q --spider http://google.com

if [ $? -eq 0 ]; then
	exit 0
else 
	exit 1
fi
