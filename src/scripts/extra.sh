#!/bin/bash

# Este script trabaja con los archivos que terminan en .jpg
# de la carpeta actual y devuelve su tamaño.

# Mostrar el tamaño de cada imagen descargada

read -p "INGRESE 1 PARA VER INFORMACION SOBRE LAS IMAGENES. INGRESE 2 PARA VER INFORMACION SOBRE LAS ETIQUETAS: " OPCION


IMG_FOUND=false
TAG_FOUND=false

if [ "$OPCION" -eq 1 ]; then

	for file in *.jpg; do
		if [ -e $file ]; then

			IMG_FOUND=true
 			FILE_INFO=$(stat -c "\e[1mNombre:\e[0m %n \n\e[1mTamaño:\e[0m %s bytes.\n\e[1mPropietario:\e[0m %U. \n\e[1mFecha de creación:\e[0m %w. \n\e[1mUltima modificación:\e[0m %y." "$file" )
			echo -e "\e[35mInformación de la imagen descargada:\e[0m"
 			echo -e "$FILE_INFO"

		elif [ "$IMG_FOUND" = false ]; then
        		echo -e "\e[1mNo se encontraron imagenes\e[0m"

		fi
	done


elif [ "$OPCION" -eq 2 ]; then
 	for etiqueta in *.tag; do
		if [ -e $etiqueta ]; then

			TAG_FOUND=true
			TAG_INFO=$(stat -c "\e[1mNombre:\e[0m %n \n\e[1mTamaño:\e[0m %s bytes.\n\e[1mPropietario:\e[0m %U. \n\e[1mFecha de creación:\e[0m %w. \n\e[1mUltima modificación:\e[0m %y." "$etiqueta" )
			echo -e "\e[35mInformación de las etiquetas:\e[0m"
                	echo -e "$TAG_INFO"

		elif [ "$TAG_FOUND" = false ]; then
                        echo -e "\e[1mNo se encontraron etiquetas\e[0m"

		fi
	done
fi

exit 0


#ideas: agregar tamaño del comprimido
