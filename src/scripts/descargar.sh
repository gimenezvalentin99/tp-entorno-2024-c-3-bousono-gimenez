#!/bin/bash

# Este script debe descargar una sola imagen desde internet en la carpeta actual.
# Puede recibir un argumento opcional indicando la clase de la imagen.
# El nombre del archivo deberá ser su suma de verificación y debe terminar en .jpg
# Asegúrese de devolver un valor de salida acorde a la situación

# Exigimos 1 solo argumento o ninguno como clase
if [ $# -gt 1 ]; then
  echo "Uso: $0 [clase de imagen]"
  exit 1
fi

# La variable CLASS va a almacenar el primer argumento pasado (si no se pasa un argumento toma el valor default)
CLASS=$1

# Damos una imagen por default
DEFAULT_IMAGE_URL="https://random-image-pepebigotes.vercel.app/api/random-image"

# Obtener la URL de la imagen según la clase proporcionada
if [ "$CLASS" != "" ]; then
  IMAGE_URL="https://loremflickr.com/512/512/$CLASS"
else
  IMAGE_URL=$DEFAULT_IMAGE_URL
fi

# Descargar la imagen
echo -e "\e[36mDescargando imagen...\e[0m"
IMAGE_NAME=$(basename "$IMAGE_URL$RANDOM")	#basename: toma una ruta completa como argumento y devuelve solo el nombre del archivo o directorio en esa ruta.
wget -O "$IMAGE_NAME" "$IMAGE_URL" &> /dev/null

# Verificar si la descarga fue exitosa
if [ $? -ne 0 ]; then
  echo "Error al descargar la imagen"
  exit 2
fi

# Calcular la suma de verificación de la imagen
CHECKSUM=$(echo "$IMAGE_NAME" | sha256sum | awk '{ print $1 }')

# Renombrar el archivo con su suma de verificación
mv "$IMAGE_NAME" "$CHECKSUM.jpg"

# Verificar si el renombrado fue exitoso
if [ $? -ne 0 ]; then
  echo "Error al renombrar la imagen"
  exit 3
fi

echo -e "\e[32mImagen descargada y guardada como $CHECKSUM.jpg\e[0m"
exit 0
