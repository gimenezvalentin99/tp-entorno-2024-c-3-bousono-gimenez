#!/bin/bash

# Este script comprime todo el contenido de la carpeta actual y además, genera
# una suma de verificación del archivo resultante en un archivo separado.


# Nombre del archivo comprimido
ARCHIVO_COMPRIMIDO="contenido_comprimido.tar.gz"
# Nombre del archivo de suma de verificación
ARCHIVO_SUMAS="suma_verificacion.sha256"

# Comprimir el contenido de la carpeta actual
tar -czf "$ARCHIVO_COMPRIMIDO" *

# Verificar si la compresión fue exitosa
if [ $? -ne 0 ]; then
    echo "Error al comprimir el contenido de la carpeta."
    exit 1
fi

# Generar la suma de verificación del archivo comprimido
sha256sum "$ARCHIVO_COMPRIMIDO" > "$ARCHIVO_SUMAS"

# Verificar si la generación de la suma de verificación fue exitosa
if [ $? -ne 0 ]; then
    echo "Error al generar la suma de verificación."
    exit 1
fi

echo "Compresión y generación de suma de verificación completadas con éxito."
exit 0
