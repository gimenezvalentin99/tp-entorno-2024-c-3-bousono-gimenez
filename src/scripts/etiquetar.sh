#!/bin/bash

# Este script trabaja sobre archivos de la carpeta actual que terminan en .jpg,
# clasificándolos usando YOLO:
#
# Debe crearse un archivo con el mismo nombre que la imagen, pero extensión .tag
# donde se guardan las etiquetas. Por ejemplo, un archivo .tag podría tener:
# 2 persons, 1 potted plant, 1 laptop, 2 books
#
# Asegúrese de devolver un valor de salida acorde a la situación.

# Procesar cada archivo .jpg en la carpeta actual
for IMG in *.jpg; do
    if [ -e "$IMG" ]; then
        # Crear nombre del archivo .tag
        TAG_FILE="${IMG%.jpg}.tag"

        # Ejecutar YOLO sobre la imagen y capturar las etiquetas
	echo -e "\e[36mEtiquetando imagenes...\e[0m"
        OUTPUT=$(yolo predict source="$IMG" 2> /dev/null)
	LABELS=$(echo "$OUTPUT" | sed -En 's/.* ([0-9]+x[0-9]+) (.*) ([0-9]+\.?[0-9]+)ms$/\2/p')

        # Guardar las etiquetas en el archivo .tag
        echo "$LABELS" > "$TAG_FILE"

        # Verificar si el archivo .tag se creó correctamente
        if [ -f "$TAG_FILE" ]; then
            echo -e "\e[32mArchivo $TAG_FILE creado exitosamente.\e[0m"
        else
            echo "Error al crear el archivo $TAG_FILE para $TAG_FILE."
            exit 2
        fi
    else
        echo "No se encontraron archivos .jpg en la carpeta actual."
        exit 3
    fi
done

echo "Clasificación completada."
exit 0

