#!/bin/bash

# Este script es un programa interactivo que no recibe argumentos.
# Debe preguntarle al usuario que etiqueta desea buscar y mostrar por
# pantalla todas las imágenes que tengan esa etiqueta.

# Pedir al usuario que ingrese la etiqueta que desea buscar
read -p "Ingrese la etiqueta de la imagen que desea ver: " ETIQUETA

# Buscar la imagen que contiene la etiqueta
ENCONTRADA=0
for TAG_FILE in *.tag; do
    if grep -q "$ETIQUETA" "$TAG_FILE"; then
        IMAGEN="${TAG_FILE%.tag}.jpg"
        echo "Mostrando la imagen $IMAGEN con la etiqueta '$ETIQUETA':"
        jp2a --fill --color "$IMAGEN"
        ENCONTRADA=1
    fi
done

# Verificar si se encontraron imágenes
if [ $ENCONTRADA -eq 0 ]; then
    echo "No se encontraron imágenes con la etiqueta '$ETIQUETA'."
fi

exit 0

